const { sin, cos, PI, atan } = Math;
const $ = (el) => Number(document.getElementById(el).value);

const toRadians = (deg) => (deg * PI) / 180;
const toDegrees = (rad) => (rad * 180) / PI;
const calculateResultant = (...forces) => {
    let res = forces.reduce(
        (resultant, x) => {
            const [force, theta] = x;
            let fx = force * cos(toRadians(theta));
            let fy = force * sin(toRadians(theta));
            resultant.Fx += fx;
            resultant.Fy += fy;
            return resultant;
        },
        { Fx: 0, Fy: 0 }
    );
    res.resultant = (res.Fx ** 2 + res.Fy ** 2) ** 0.5;
    res.theta = toDegrees(atan(res.Fy / res.Fx));
    for (let prop in res) {
        res[prop] = res[prop].toFixed(3);
    }
    return res;
};
const handleSubmit = (e) => {
    const input = {
        f1: { magnitude: $("F1") + ($("mis") % 100), theta: $("A1") },
        f2: { magnitude: $("F2") + ($("mis") % 100), theta: $("A2") },
        f3:
            $("F3") != 0
                ? {
                      magnitude: $("F3") + ($("mis") % 100),
                      theta: $("A3"),
                  }
                : { magnitude: 0, theta: 0 },
    };
    let obj = calculateResultant(
        ...Object.values(input).map((x) => Object.values(x))
    );

    document.getElementById("res").innerHTML = obj.resultant;
    document.getElementById("Fx").innerHTML = obj.Fx;
    document.getElementById("Fy").innerHTML = obj.Fy;
    document.getElementById("alpha").innerHTML = obj.theta;
    console.log(obj.theta);
    document.getElementById("result").style.display = "flex";
};
